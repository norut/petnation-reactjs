import axios from 'axios';

export const apiCEP = axios.create({
  baseURL: 'https://ws.apicep.com/cep/',
});
