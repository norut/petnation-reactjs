import axios from 'axios';

export const apiMock = axios.create({
  baseURL: 'https://6161a5d337492500176313d2.mockapi.io/petnation/v1/',
});
