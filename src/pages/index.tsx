/* eslint-disable @next/next/no-img-element */
import Head from 'next/head';
import { CardEvent } from '../components/CardEvent';
import { HowItWorks } from '../components/HowItWorks';
import { WhatIs } from '../components/WhatIs';

import styles from './home.module.scss';

export default function Home() {
  return (
    <>
      <Head>
        <title>Home | PETNation</title>
      </Head>

      <main className={styles.mainContainer}>
        <WhatIs />
        <HowItWorks />

        <section className={styles.ongsEventContainer}>
          <img src="/svgs/artEvent-main.svg" alt="art" />
          <img src="/svgs/ongsEventIcon.svg" alt="Icon Event" />
          <h1>
            ONG&rsquo;s em <span>evento</span>
          </h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua.
          </p>

          <ul>
            <CardEvent />
            <CardEvent />
            <CardEvent />
          </ul>
        </section>
      </main>
    </>
  );
}
