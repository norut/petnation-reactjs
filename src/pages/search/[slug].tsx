/* eslint-disable @next/next/no-img-element */
import Head from 'next/head';
import { IoClose } from 'react-icons/io5';

import { SearchBar } from '../../components/SearchBar';
import { ButtonFilters } from '../../components/ButtonFilters';
import { DropDownLocation } from '../../components/DropdownLocation';
import { Dropdown } from '../../components/Dropdown';

import styles from './styles.module.scss';
import { useCallback, useState } from 'react';
import { Card } from '../../components/Card';
import { GetServerSideProps } from 'next';
import { apiMock } from '../../services/apiMock';

interface AnimalData {
  name: string;
  breed: string;
  sex: string;
  age: number;
  user_id: string;
  id: string;
}

interface SearchPageProps {
  animalList: AnimalData[];
}

const SearchPage = ({ animalList }: SearchPageProps) => {
  const [isVisibleFilters, setIsVisibleFilters] = useState(false);

  const handleSetFiltersVisible = useCallback(() => {
    setIsVisibleFilters((oldValue) => !oldValue);
  }, []);

  return (
    <>
      <Head>
        <title>Slug | PETNation</title>
      </Head>

      <main className={styles.searchPageContainer}>
        <img
          src="/svgs/artSearch-top.svg"
          alt="art"
          className={styles.artTop}
        />

        <div className={styles.searchPageContent}>
          <div>
            <SearchBar placeholder="Procure por Ongs ou Pessoas..." />
            <ButtonFilters
              onSetVisibleFilters={handleSetFiltersVisible}
              isVisibleFilters={isVisibleFilters}
            />
            {/* <DropDownLocation /> */}
          </div>

          <img
            src="/svgs/artSearch-main.svg"
            alt="art"
            className={styles.artBottom}
          />

          <div
            className={`${styles.filtersList} ${
              isVisibleFilters && styles.filtersListVisible
            }`}
          >
            {isVisibleFilters ? (
              <>
                <Dropdown
                  placeholder="Espécie"
                  content={['test']}
                  width="150px"
                />
                <Dropdown
                  placeholder="Gênero"
                  content={['test']}
                  width="150px"
                />
                <Dropdown
                  placeholder="Estado"
                  content={['test']}
                  width="100px"
                />
                <Dropdown
                  placeholder="Cidade"
                  content={['test']}
                  width="250px"
                />

                <button>
                  Limpar <IoClose />{' '}
                </button>
              </>
            ) : (
              <span>Cachorro - Pintcher - Macho - Artur Nogueira - SP</span>
            )}
          </div>

          <ul className={styles.cardsList}>
            {animalList.map((animal) => (
              <Card
                key={animal.id}
                cardType="dog"
                genre={animal.sex}
                image_url="https://images.unsplash.com/photo-1534361960057-19889db9621e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80"
                title={animal.name}
                age={animal.age}
              />
            ))}
            {/* <Card
              cardType="dog"
              genre="Femea"
              image_url="https://images.unsplash.com/photo-1534361960057-19889db9621e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80"
              title="Diana"
              age={5}
            />
            <Card
              cardType="ong"
              image_url="https://images.unsplash.com/photo-1623387641168-d9803ddd3f35?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80"
              title="Amigo da Onça"
              ongDescription="Cães e Gatos"
            /> */}
          </ul>
        </div>
      </main>
    </>
  );
};

export default SearchPage;

export const getServerSideProps: GetServerSideProps = async () => {
  const animalList = (await await apiMock.get<AnimalData[]>('animals')).data;

  return {
    props: {
      animalList,
    },
  };
};
