/* eslint-disable @next/next/no-img-element */
import Head from 'next/head';
import { useState } from 'react';
import { SignInForm } from '../../components/Forms/SignIn';
import { SignUpForm } from '../../components/Forms/SignUp';

import styles from './styles.module.scss';

const Login = () => {
  const [isSignUp, setIsSignUp] = useState(false);

  return (
    <>
      <Head>
        <title>Entrar | PETNation</title>
      </Head>

      <main className={styles.authPageContainer}>
        <img src="./svgs/artAuth-top.svg" alt="art" className={styles.artTop} />
        <img
          src="./svgs/artAuth-bottom.svg"
          alt="art"
          className={styles.artBottom}
        />
        <img
          src="./svgs/artAuth-main.svg"
          alt="art"
          className={styles.artMain}
        />

        <div className={styles.formContainer}>
          <section className={styles.infoSection}>
            {isSignUp ? (
              <>
                <h1>Faça parte da nossa comunidade!</h1>
                <p>
                  Exponha sua ONG para que as pessoa possam fazer doações ou
                  então coloque animais para a adoção.
                </p>
              </>
            ) : (
              <>
                <h1>Bem Vindo!</h1>
                <p>
                  Ficamos felizes que tenha voltado a utilizar dos nossos
                  serviços
                </p>
              </>
            )}
          </section>
          <section className={styles.formSection}>
            {isSignUp ? (
              <SignUpForm onSetSignUpForm={setIsSignUp} />
            ) : (
              <SignInForm onSetSignUpForm={setIsSignUp} />
            )}
          </section>
        </div>
      </main>
    </>
  );
};

export default Login;
