/* eslint-disable @next/next/no-img-element */
import Head from 'next/head';
import { useCallback, useState } from 'react';
import {
  HiOutlineMail,
  HiOutlinePhone,
  HiOutlineLocationMarker,
} from 'react-icons/hi';
import { NewPetModal } from '../../components/NewPetModal';

import styles from './styles.module.scss';

const Profile = () => {
  const [newPetModalIsOpen, setNewPetModalIsOpen] = useState(false);

  const handleCloseNewPetModal = useCallback(() => {
    setNewPetModalIsOpen(false);
  }, []);

  return (
    <>
      <Head>
        <title>Marcos Vinicios | PETNation</title>
      </Head>

      <main className={styles.profilePageContainer}>
        <img
          src="/svgs/artProfile-top.svg"
          alt="art"
          className={styles.artTop}
        />
        <img
          src="/svgs/artProfile-main.svg"
          alt="art"
          className={styles.artMain}
        />

        <div className={styles.profileContainer}>
          <div style={{ backgroundImage: 'url(/images/marcos.jpg)' }} />
          <h2>Marcos Vincios</h2>
          <p>Uma breve descrição</p>

          <div className={styles.boxInfoProfile}>
            <span>
              <HiOutlineMail /> marcosviniciosdev13@gmail.com
            </span>
            <span>
              <HiOutlinePhone /> (19) 9 9693-2478
            </span>
            <span>
              <HiOutlineLocationMarker /> Artur Nogueira - SP
            </span>
          </div>

          <img
            src="/svgs/artProfile-bottom.svg"
            alt="art"
            className={styles.artBottom}
          />
        </div>

        <div className={styles.publicationsContainer}>
          <div className={styles.navigationPublicationsBar}>
            <span className={styles.activeNavigation}>Publicações</span>
            <span>Cães</span>
            <span>Gatos</span>
          </div>

          <div
            className={styles.addNewPet}
            onClick={() => setNewPetModalIsOpen(true)}
          >
            <img src="/svgs/addNewPet.svg" alt="icone adicionar novo pet" />
            <span>Colocar animalzinho para adoção</span>
          </div>
        </div>

        <div className={styles.suggestedContainer}>
          <h2>Todos</h2>

          <div>
            <div
              style={{
                backgroundImage:
                  'url(https://images.unsplash.com/photo-1588943211346-0908a1fb0b01?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=735&q=80)',
              }}
            />{' '}
            <span>Bernardo</span>
          </div>
          <div>
            <div
              style={{
                backgroundImage:
                  'url(https://images.unsplash.com/photo-1529429617124-95b109e86bb8?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=735&q=80)',
              }}
            />{' '}
            <span> Rex </span>
          </div>
          <div>
            <div
              style={{
                backgroundImage:
                  'url(https://images.unsplash.com/photo-1558788353-f76d92427f16?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=738&q=80)',
              }}
            />{' '}
            <span> Alfredo </span>
          </div>
        </div>
      </main>
      <NewPetModal
        isOpen={newPetModalIsOpen}
        onRequestClose={handleCloseNewPetModal}
      />
    </>
  );
};

export default Profile;
