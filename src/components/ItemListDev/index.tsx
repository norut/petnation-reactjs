import styles from './styles.module.scss';

export const ItemListDev = () => {
  return (
    <li className={styles.ItemListContainer}>
      <div>
        <div />
      </div>
      <h3>Marcos Vinicios</h3>
      <span>(Front-end)</span>
    </li>
  );
};
