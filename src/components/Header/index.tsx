import { useRouter } from 'next/router';
import Link from 'next/link';

import styles from './styles.module.scss';

export const Header = () => {
  const router = useRouter();

  return (
    <header className={styles.headerContainer}>
      <div className={styles.headerContent}>
        <img src="/svgs/Logo.svg" alt="logo" />

        <nav>
          <Link href="/">Home</Link>
          <Link href="/search/ongs">Doar</Link>
          <Link href="/search/animals">Adotar</Link>
        </nav>

        <button onClick={() => router.push('/authentication')}>Entrar</button>
      </div>
    </header>
  );
};
