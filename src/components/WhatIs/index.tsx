/* eslint-disable @next/next/no-img-element */
import styles from './styles.module.scss';

export const WhatIs = () => {
  return (
    <section className={styles.whatIsSession}>
      <img src="/svgs/artHome-top.svg" alt="art" className={styles.artTop} />
      <div className={styles.whatIsContent}>
        <img src="/svgs/artHome-left.svg" alt="art" />
        <h1>
          O que é PET<span>Nation</span>?
        </h1>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt .
        </p>
        <div>
          <button>Fazer Doação</button>
          <button>Saber Mais</button>
        </div>
      </div>
      <img
        src="/images/artHome-main.png"
        alt="art"
        className={styles.artMain}
      />
    </section>
  );
};
