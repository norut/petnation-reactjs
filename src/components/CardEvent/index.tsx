import { BsArrowRight } from 'react-icons/bs';

import styles from './styles.module.scss';

export const CardEvent = () => {
  return (
    <li className={styles.cardEventContainer}>
      <img src="/images/image.png" alt="Image" />

      <div>
        <h3>Evento</h3>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing eli..Lorem ipsum
          dolor sit amet...
        </p>
        <button>
          <BsArrowRight />
        </button>
      </div>
    </li>
  );
};
