import { useEffect } from 'react';
import { AddNewPetForm } from '../Forms/AddNewPet';
import styles from './styles.module.scss';

interface NewPetModalProps {
  isOpen: Boolean;
  onRequestClose: () => void;
}

export const NewPetModal = ({ isOpen, onRequestClose }: NewPetModalProps) => {
  useEffect(() => {
    const body = document.querySelector('body');
    if (isOpen) {
      body.style.overflowY = 'hidden';
    } else {
      body.style.overflowY = 'auto';
    }
  }, [isOpen]);

  if (isOpen) {
    return (
      <div className={styles.overlay}>
        <div className={styles.overlayClose} onClick={onRequestClose} />
        <div className={styles.formAddPet}>
          <span>Novo Pet para adoção</span>

          <AddNewPetForm />
        </div>
      </div>
    );
  } else {
    return <></>;
  }
};
