import styles from './styles.module.scss';

export const HowItWorks = () => {
  return (
    <section className={styles.howItWorksContainer}>
      <img src="/svgs/artHow-left.svg" alt="art" />
      <div className={styles.howItWorksContent}>
        <img src="/svgs/artHow-right.svg" alt="art" />

        <h1>
          E como <span>funciona</span>?
        </h1>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </p>
        <button>Cadastre sua ONG</button>
      </div>
    </section>
  );
};
