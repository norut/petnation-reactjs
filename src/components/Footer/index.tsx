import { ItemListDev } from '../ItemListDev';

import styles from './styles.module.scss';

export const Footer = () => {
  return (
    <footer className={styles.footerContainer}>
      <img src="/svgs/Logo.svg" alt="logo" />

      <div className={styles.footerContent}>
        <h1>Desenvolvedores</h1>

        <ul>
          <ItemListDev />
        </ul>
      </div>
    </footer>
  );
};
