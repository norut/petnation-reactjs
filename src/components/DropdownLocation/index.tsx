import { useState } from 'react';
import { IoLocationOutline } from 'react-icons/io5';

import styles from './styles.module.scss';

export const DropDownLocation = () => {
  const [isDropdownVisible, setIsDropdownVisible] = useState(false);

  return (
    <div
      className={`${styles.selectLocationContainer} ${
        isDropdownVisible && styles.dropdownListVisible
      }`}
    >
      <span>
        <span>
          <IoLocationOutline />
        </span>
        <input
          type="text"
          placeholder="Localização"
          onFocus={() => setIsDropdownVisible(true)}
          onBlur={() => setIsDropdownVisible(false)}
        />
      </span>

      <ul>
        <li>Filtros</li>
        <li>Filtros</li>
        <li>Filtros</li>
        <li>Filtros</li>
        <li>Filtros</li>
        <li>Filtros</li>
        <li>Filtros</li>
        <li>Filtros</li>
        <li>Filtros</li>
        <li>Filtros</li>
        <li>Filtros</li>
        <li>Filtros</li>
      </ul>
    </div>
  );
};
