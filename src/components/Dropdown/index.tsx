import { useState } from 'react';
import { FaChevronDown } from 'react-icons/fa';

import styles from './styles.module.scss';

interface DropdownProps {
  placeholder: string;
  content: string[];
  width?: string;
}

export const Dropdown = ({
  content,
  placeholder,
  width = '100px',
}: DropdownProps) => {
  const [isVisibleDropList, setIsVisibleDropList] = useState(false);

  return (
    <div
      className={`${styles.dropdownContainer} ${
        isVisibleDropList && styles.dropListVisible
      }`}
      style={{ width }}
    >
      <span>
        <input
          type="text"
          placeholder={placeholder}
          readOnly
          onFocus={() => setIsVisibleDropList(true)}
          onBlur={() => setIsVisibleDropList(false)}
        />
        {/* Colocar position absolute e por cima do input text
        para pegar o focus quando ser clicado tbm */}
        <FaChevronDown />
      </span>

      <ul>
        {content.map((item) => (
          <li key={item}>{item}</li>
        ))}
      </ul>
    </div>
  );
};
