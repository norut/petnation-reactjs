import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';

import { Input } from '../../Input';

import styles from './styles.module.scss';

interface IFormInputs {
  email: string;
  password: string;
}

interface SignInFormProps {
  onSetSignUpForm: (value: boolean) => void;
}

const sigInFormSchema = yup.object().shape({
  email: yup.string().required('E-mail obrigatório').email('E-mail inválido'),
  password: yup.string().required('Senha obrigatória'),
});

export const SignInForm = ({ onSetSignUpForm }: SignInFormProps) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IFormInputs>({
    resolver: yupResolver(sigInFormSchema),
  });

  const handleSignIn = (data: IFormInputs) => {
    alert(JSON.stringify(data));
  };

  return (
    <form
      onSubmit={handleSubmit(handleSignIn)}
      className={styles.formContainerSignIn}
    >
      <h1>Acesse sua conta</h1>

      <Input
        name="email"
        type="email"
        placeholder="E-mail"
        error={errors.email}
        {...register('email')}
      />
      <Input
        name="password"
        type="password"
        placeholder="Senha"
        error={errors.password}
        {...register('password')}
      />

      <button>Continuar</button>

      <span>
        Não possui conta?{' '}
        <span onClick={() => onSetSignUpForm(true)}>Cadastre-se</span>
      </span>
    </form>
  );
};
