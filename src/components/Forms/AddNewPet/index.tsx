import { Dropdown } from '../../Dropdown';
import { Input } from '../../Input';

import styles from './styles.module.scss';

/* eslint-disable @next/next/no-img-element */
export const AddNewPetForm = () => {
  return (
    <form className={styles.formContainer}>
      <div className={styles.inputPhoto}>
        <img src="/svgs/iconAddPhoto.svg" alt="icone adicionar nova foto" />
        <span>Escolher foto do animal</span>
      </div>

      <div className={styles.anotherInputsContainer}>
        <Input placeholder="Nome completo" />

        <div className={styles.partitionContainer}>
          <Input placeholder="Idade" />
          <Dropdown placeholder="Gênero" content={['test']} width="50%" />
        </div>

        <div className={styles.partitionContainer}>
          <Input placeholder="Espécie" />
          <Input placeholder="Raça" />
        </div>

        <div className={styles.buttonsContainer}>
          <button type="button">Cancelar</button>
          <button type="submit">Cadastrar</button>
        </div>
      </div>
    </form>
  );
};
