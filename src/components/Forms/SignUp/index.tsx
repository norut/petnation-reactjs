import { useState } from 'react';
import { BsArrowRightShort, BsArrowLeftShort } from 'react-icons/bs';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';

import { Input } from '../../Input';
import styles from './styles.module.scss';
import { apiCEP } from '../../../services/apiCep';
import { ApiCEPResponse, IFormInputs } from './types';
import { sigUpFormSchema } from './schema';

interface SignUpFormProps {
  onSetSignUpForm;
}

export const SignUpForm = ({ onSetSignUpForm }: SignUpFormProps) => {
  const [isComplementForm, setIsComplementForm] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    setError,
  } = useForm<IFormInputs>({
    resolver: yupResolver(sigUpFormSchema),
  });

  const handleSignUp = (data: IFormInputs) => {
    console.log(data);
  };

  const autoFillFields = async (cep: string) => {
    const data = await (await apiCEP.get<ApiCEPResponse>(`${cep}.json`)).data;

    if (data.message) {
      setError(
        'cep',
        {
          type: 'validate',
          message: data.message,
        },
        {
          shouldFocus: true,
        }
      );
    } else {
      setValue('address', data.address);
      setValue('city', data.city);
      setValue('state', data.state);
      setValue('neighborhood', data.district);
    }
  };

  return (
    <>
      <button
        className={`${styles.button} ${
          isComplementForm ? styles.buttonReturn : styles.buttonNext
        }`}
        onClick={() => setIsComplementForm(!isComplementForm)}
      >
        {isComplementForm ? (
          <BsArrowLeftShort />
        ) : (
          <>
            Continuar
            <BsArrowRightShort />
          </>
        )}
      </button>
      <form
        className={styles.formContainerSignUp}
        onSubmit={handleSubmit(handleSignUp)}
      >
        <h1>Cadastre-se</h1>

        <section
          className={
            isComplementForm ? styles.invisibleForm : styles.formVisible
          }
        >
          <Input
            placeholder="Nome Completo"
            name="name"
            {...register('name')}
            error={errors.name}
          />
          <Input
            placeholder="E-mail"
            type="email"
            name="email"
            {...register('email')}
            error={errors.email}
          />

          <Input
            placeholder="Senha"
            type="password"
            name="password"
            {...register('password')}
            error={errors.password}
          />
          <Input
            placeholder="Confirmar senha"
            type="password"
            name="password_confirmation"
            {...register('password_confirmation')}
            error={errors.password_confirmation}
          />
        </section>

        <section
          className={
            !isComplementForm ? styles.invisibleForm : styles.formVisible
          }
        >
          <Input
            placeholder="CEP"
            name="cep"
            {...register('cep')}
            error={errors.cep}
            onBlur={(e) => autoFillFields(e.target.value)}
          />

          <Input
            placeholder="Estado"
            name="state"
            {...register('state')}
            error={errors.state}
          />
          <Input
            placeholder="Cidade"
            name="city"
            {...register('city')}
            error={errors.city}
          />
          <Input
            placeholder="Bairro"
            name="neighborhood"
            {...register('neighborhood')}
            error={errors.neighborhood}
          />

          <div className={styles.boxStreet}>
            <Input
              placeholder="Endereço"
              name="address"
              {...register('address')}
              error={errors.address}
            />
            <Input
              placeholder="Nº"
              name="house_number"
              {...register('house_number')}
              error={errors.house_number}
            />
          </div>
        </section>

        <div className={styles.footerForm}>
          <span>
            Já possui conta?{' '}
            <span onClick={() => onSetSignUpForm(false)}>Acesse</span>
          </span>

          {isComplementForm && (
            <button className={styles.button}>Cadastrar</button>
          )}
        </div>
      </form>
    </>
  );
};
