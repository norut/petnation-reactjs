export interface IFormInputs {
  name: string;
  email: string;
  password: string;
  password_confirmation: string;
  cep: number;
  state: string;
  city: string;
  neighborhood: string;
  address: string;
  house_number: number;
}

export interface ApiCEPResponse {
  message?: string;
  address: string;
  city: string;
  district: string;
  state: string;
}
