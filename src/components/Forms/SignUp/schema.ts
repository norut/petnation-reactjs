import * as yup from 'yup';

export const sigUpFormSchema = yup.object().shape({
  name: yup.string().required('Nome obrigatório'),
  email: yup.string().required('E-mail obrigatório').email('E-mail inválido'),
  password: yup
    .string()
    .required('Senha obrigatória')
    .min(6, 'No mínimo 6 caracteres'),
  password_confirmation: yup
    .string()
    .oneOf([null, yup.ref('password')], 'As senhas precisam ser iguais'),
  cep: yup.string().required('CEP obrigatório'),
  state: yup.string().required('Estado obrigatório'),
  city: yup.string().required('Cidade obrigatória'),
  neighborhood: yup.string().required('Bairro obrigatório'),
  address: yup.string().required('Endereço obrigatório'),
  house_number: yup.string().required('Numero obrigatório'),
});
