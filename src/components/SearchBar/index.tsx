import { GoSearch } from 'react-icons/go';

import styles from './styles.module.scss';

interface SearchBarProps {
  placeholder: string;
}

export const SearchBar = ({ placeholder = 'Test' }: SearchBarProps) => {
  return (
    <span className={styles.searchBarContainer}>
      <button>
        <GoSearch />
      </button>

      <input type="text" placeholder={placeholder} />
    </span>
  );
};
