import { IoFilter } from 'react-icons/io5';

import styles from './styles.module.scss';

interface ButtonFiltersProps {
  onSetVisibleFilters: () => void;
  isVisibleFilters: boolean;
}

export const ButtonFilters = ({
  onSetVisibleFilters,
  isVisibleFilters,
}: ButtonFiltersProps) => {
  return (
    <div
      className={`${styles.filtersButtonContainer} ${
        isVisibleFilters && styles.filtersVisible
      }`}
      onClick={onSetVisibleFilters}
    >
      <span>
        <span>
          <IoFilter />
        </span>
        {isVisibleFilters
          ? 'Cachorro - Pintcher - Macho - Artur Nogueira - SP'
          : 'Filtros'}
      </span>
    </div>
  );
};
