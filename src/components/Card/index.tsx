/* eslint-disable @next/next/no-img-element */
import styles from './styles.module.scss';

interface CardProps {
  cardType: string;
  genre?: string;
  image_url: string;
  title: string;
  ongDescription?: string;
  age?: number;
}

export const Card = ({
  cardType,
  genre,
  image_url,
  age,
  ongDescription,
  title,
}: CardProps) => {
  const backColor = {
    cat: '#29C8D2',
    dog: '#2995D2',
    ong: '#B768DC',
  };

  return (
    <li className={styles.cardContainer}>
      <div
        style={{
          backgroundImage: `url(${image_url})`,
        }}
        className={styles.cardImage}
      />
      <span
        style={{ backgroundColor: backColor[cardType] }}
        className={styles.typeCard}
      >
        <img src={`/svgs/${cardType}.svg`} alt="" />
      </span>
      <div className={styles.cardContent}>
        <h2>{title}</h2>
        <span>
          {age && `${age} Aninhos`}
          {ongDescription && ongDescription}
          <span style={{ color: genre === 'Macho' ? '#2995D2' : '#D229B7' }}>
            {genre && genre}
          </span>
        </span>
      </div>
    </li>
  );
};
